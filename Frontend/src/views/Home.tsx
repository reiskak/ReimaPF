import { Box, Container } from "@mui/material";
import { useRef } from "react";
import Info from "../components/Info";
import Nav from "../components/Nav";
import Projects from "../components/Projects";
import Studies from "../components/Studies";

function Home() {
  const infoRef = useRef(null);
  const projectsRef = useRef(null);
  const studiesRef = useRef(null);
  return (
    <Box>
      <Nav
        SchoolRef={studiesRef}
        InfoRef={infoRef}
        ProjectsRef={projectsRef}
      ></Nav>
      <Container sx={{ padding: 2 }} maxWidth="lg">
        <Box ref={infoRef}>
          <Info />
        </Box>
        <Box ref={studiesRef}>
          <Studies />
        </Box>
        <Box ref={projectsRef}>
          <Projects />
        </Box>
      </Container>
    </Box>
  );
}

export default Home;

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Chip,
  Grid,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { styles, colors } from "../css/style";
import { getProjectsFromDatabase } from "../firebase/firebase";

interface ProjectData {
  title: string;
  image?: string;
  description: string;
  languages: string[];
  git?: string;
  demo?: string;
}

interface ProjectItemProps {
  data: ProjectData;
}

function ProjectItem({ data }: ProjectItemProps) {
  return (
    <Grid item xs={12} md={4}>
      <Card
        sx={{
          ...{
            height: "100%",
            display: "flex",
            flexDirection: "column",
            backgroundColor: colors.neutral,
          },
        }}
      >
        <CardHeader title={data.title} />
        <CardMedia
          sx={{
            height: "auto",
            maxHeight: "200px",
          }}
          component="img"
          image={data.image}
        />
        <CardContent>
          <Box sx={{ flexWrap: "wrap" }}>
            {data.languages.map((language, index) => {
              return <Chip key={index} label={language} sx={{ margin: 0.2 }} />;
            })}
          </Box>
          <Typography>{data.description}</Typography>
        </CardContent>
        <div style={{ flexGrow: 1 }}></div>
        <CardActions>
          {data.git && (
            <Button target="_blank" href={data.git} size="small">
              Gitlab
            </Button>
          )}
          {data.demo && (
            <Button target="_blank" href={data.demo} size="small">
              Demo
            </Button>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
}

function Projects() {
  useEffect(() => {
    getProjectsFromDatabase().then((res) => {
      if (!res) return;
      let projects = [...res.git, ...(res.manual ?? [])];
      projects.sort((a, b) => b.priority - a.priority);
      setProjects(projects);
    });
  }, []);

  const [projects, setProjects] = useState([]);

  return (
    <Card sx={{ ...styles.card, ...{ marginTop: 2, padding: 1 } }}>
      <Grid container spacing={2}>
        <Grid justifyItems="center" alignItems="center" item xs={12}>
          <CardContent
            style={{
              alignItems: "center",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography sx={styles.title} variant="h4" noWrap>
              Projektit
            </Typography>
            <Typography>
              Projektit osiosta löydät näytteitä koulu- ja
              harrasteprojekteistani. Näiden lisäksi minulla on paljon
              itseopiskeluprojekteja, kuten MMORPG server emulaattoria, peli-,
              älykoti-, kryptoprojekteja, joita en ole yleiseen gitlabiin vielä
              laittanut.
            </Typography>
          </CardContent>
        </Grid>
        {projects.map((data, index) => (
          <ProjectItem key={index} data={data} />
        ))}
      </Grid>
    </Card>
  );
}

export default Projects;

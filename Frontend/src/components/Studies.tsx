import { Card, Box, Grid, Typography, Link } from "@mui/material";
import React from "react";
import { styles } from "../css/style";

function Studies() {
  return (
    <Card sx={{ ...styles.card, ...{ marginTop: 2, padding: 2 } }}>
      <Grid container spacing={2} direction="row-reverse">
        <Grid justifyItems="center" alignItems="center" item xs={12}>
          <Box
            style={{ justifyContent: "center", display: "flex", padding: 0 }}
          >
            <Typography sx={styles.title} variant="h4" noWrap>
              KOULU
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Typography gutterBottom>
            Aloitin opiskelut 2019 syksyllä ja olen edennyt opinnoissani
            suunnitellusti. Koulussa olen pystynyt täyttämään aiemman
            ohjelmoinnin itseopiskelun jättämiä tietoaukkoja. Olen myös ollut
            erittäin motivoitunut opiskelemaan ja tekemään tästä alasta
            itselleni ammatin, josta kertovat myös erinomaiset arvosanat.
          </Typography>
          <Typography gutterBottom>
            Itselle erityisen mielekkäistä kursseista voisin mainita Advanced
            Programming moduulin, peliohjelmoinnin ja Marko Rintamäen pitämät
            projektikurssit. Lisäksi tänä keväänä on menossa mielenkiintoinen
            Enterprise applications moduuli, johon kuuluvat advanced databases-
            ja service oriented applications kurssit.
          </Typography>
          <Link
            target="_blank"
            href="https://student.labranet.jamk.fi/~M3074/opintosuoritusote.pdf"
          >
            Arvosanojani pääset katsomaan opintosuoritusotteestani.
          </Link>
        </Grid>
      </Grid>
    </Card>
  );
}

export default Studies;

import { AppBar, Box, Button, Container } from "@mui/material";
import React from "react";
import { styles } from "../css/style";

interface NavProps {
  InfoRef: React.MutableRefObject<any>;
  SchoolRef: React.MutableRefObject<any>;
  ProjectsRef: React.MutableRefObject<any>;
}

function Nav(props: NavProps) {
  return (
    <AppBar position="static" sx={{ backgroundColor: styles.headerBg }}>
      <Container
        sx={{ display: "flex", justifyContent: "center" }}
        maxWidth="xl"
      >
        <Box
          sx={{
            flexGrow: 1,
            display: "flex",
            justifySelf: "center",
          }}
        >
          <Button
            sx={styles.headerBtn}
            onClick={() => {
              props.InfoRef.current.scrollIntoView();
            }}
          >
            INFO
          </Button>
          <Button
            sx={styles.headerBtn}
            onClick={() => {
              props.SchoolRef.current.scrollIntoView();
            }}
          >
            KOULUTUS
          </Button>
          <Button
            sx={styles.headerBtn}
            onClick={() => {
              props.ProjectsRef.current.scrollIntoView();
            }}
          >
            PROJEKTIT
          </Button>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifySelf: "flex-end",
          }}
        >
          <Button
            href="https://gitlab.com/reiskak"
            target="_blank"
            sx={styles.headerBtn}
          >
            <i className={"devicon-gitlab-plain"} style={{ fontSize: 25 }}></i>
          </Button>
          <Button
            href="https://www.linkedin.com/in/reima-ojala/"
            target="_blank"
            sx={styles.headerBtn}
          >
            <i
              className={"devicon-linkedin-plain"}
              style={{ fontSize: 25 }}
            ></i>
          </Button>
        </Box>
      </Container>
    </AppBar>
  );
}

export default Nav;

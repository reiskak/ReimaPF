import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Tooltip,
  Typography,
} from "@mui/material";
import ContactPhoneIcon from "@mui/icons-material/ContactPhone";
import ContactMailIcon from "@mui/icons-material/ContactMail";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import { styles, colors } from "../css/style";
function Info() {
  const languages = [
    { name: "C#", icon: "csharp-plain" },
    { name: "JavaScript", icon: "javascript-plain" },
    { name: "React", icon: "react-original" },
    { name: "NodeJS", icon: "nodejs-plain" },
    { name: "Elixir", icon: "elixir-plain" },
    { name: "Docker", icon: "docker-plain" },
    { name: "Firebase", icon: "firebase-plain" },
    { name: "Git", icon: "git-plain" },
    { name: "Unity", icon: "unity-original" },
    { name: "VueJS", icon: "vuejs-plain" },
  ];

  return (
    <Card sx={styles.card}>
      <Grid marginTop={1} container spacing={2} direction="row-reverse">
        <Grid justifyItems="center" alignItems="center" item xs={12}>
          <CardContent
            style={{ justifyContent: "center", display: "flex", padding: 0 }}
          >
            <Typography sx={styles.title} variant="h4" noWrap>
              INFO
            </Typography>
          </CardContent>
        </Grid>
        <Grid item xs={12} md={3}>
          <Box>
            <Avatar
              sx={{ width: "100%", height: "50%" }}
              src="/img/reima.png"
            />
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              <PermIdentityIcon sx={{ marginRight: 1 }} />
              <Typography>Reima Ojala</Typography>
            </Box>
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              <ContactMailIcon sx={{ marginRight: 1 }} />
              <Typography>reima.m.ojala@gmail.com</Typography>
            </Box>
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              <ContactPhoneIcon sx={{ marginRight: 1 }} />
              <Typography>040-7731597</Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={9}>
          <Box>
            <Typography sx={styles.title} variant="h5" noWrap gutterBottom>
              Kuka?
            </Typography>
            <Typography gutterBottom>
              Moi, olen Reima Ojala. Opiskelen tieto- ja viestintätekniikan
              insinööriopintoja Jyväskylän Ammattikorkeakoulussa. Aloitin
              opintoni 2019 ja tavoitteenani on valmistua vuonna 2023. Olen
              harrastanut kaikkea tietotekniikkaan liittyvää jo nuoresta ja
              tavoitteenani on tehdä tästä itselleni ammatti. Erityisesti pidän
              ongelmanratkaisusta, joten päädyin erikoistumaan
              ohjelmistotekniikkaan.
            </Typography>
            <Typography sx={styles.title} variant="h5" noWrap gutterBottom>
              Mitä osaan?
            </Typography>
            <Typography gutterBottom>
              Olen vuosien varrella oppinut harrastuksen ja koulun kautta useita
              eri ohjelmointikieliä, projektityöskentelyä ja erilaisia
              tietotekniikan työkaluja. Vahvimmat ohjelmointikieleni ovat C# ja
              Javascript, mutta opettelen mielelläni uusia tekniikoita ja
              tykkään haastaa itseäni. Tarkempaa näyttöä osaamisesta löydät
              portfolion projektit osiosta.
            </Typography>
            <Typography sx={styles.title} variant="h5" noWrap gutterBottom>
              Mitä haluaisin?
            </Typography>
            <Typography gutterBottom>
              Etsin tällä hetkellä harjoittelupaikkaa, jossa voisin haastaa
              itseäni, laajentaa kokemustani ja mahdollisesti työllistyä
              harjoittelun jälkeen. Olen erityisen kiinnostunut full stack- ja
              backend kehityksestä, koska rakastan ideoiden ja konseptien
              muuntamisesta toimiviksi ohjelmistokokonaisuuksiksi.
            </Typography>

            <Grid container spacing={1} sx={{ marginTop: 2 }}>
              {languages.map((language, index) => {
                return (
                  <Grid key={index} item xs={2} md={1}>
                    <Tooltip title={language.name}>
                      <i
                        className={"devicon-" + language.icon}
                        style={{ fontSize: 35, color: colors.oxblood }}
                      ></i>
                    </Tooltip>
                  </Grid>
                );
              })}
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
}

export default Info;

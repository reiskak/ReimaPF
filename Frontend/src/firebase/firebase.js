
// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";
import { getDatabase, child, ref, get, connectDatabaseEmulator } from "firebase/database";
import { getAnalytics, isSupported } from "firebase/analytics";
import firebaseConfig from "./firebaseConfig";

// Initialize Firebase

const app = initializeApp(firebaseConfig);

let analytics = null;

isSupported().then((isSupported) => {
    if (isSupported) {
        analytics = getAnalytics(app)
    }
})

const database = getDatabase(app);

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    console.log("DEV MODE");
    connectDatabaseEmulator(database, "localhost", 9000);
}


export function getProjectsFromDatabase(params) {
    return get(child(ref(database), 'projects')).then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val();
        } else {
            console.log("No data available");
            return null;
        }
    }).catch((error) => {
        console.error(error);
        return null;
    });
}



export default app;
import React from "react";
import "./App.css";
import { styles } from "./css/style";
import Home from "./views/Home";
import app from "./firebase/firebase";

function App() {
  return (
    <div style={styles.background}>
      <Home></Home>
    </div>
  );
}

export default App;

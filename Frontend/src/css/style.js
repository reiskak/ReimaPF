export const colors = {
    neutral: "#EFEFEF",
    carbon: "#A9A9A9",
    sky: "#CAEBF2",
    grain: "#D7CEC7",
    oxblood: "#76323F",
    watermelon: "#FF3B3F"
};

export const styles = {
    card: {
        backgroundColor: colors.neutral,
        padding: 2,
    },
    headerBg: {
        backgroundColor: colors.carbon
    },
    headerBtn: {
        color: colors.oxblood,
        my: 2,
        fontSize: 20,
        fontFamily: 'Bangers',
        display: "block",
        minWidth: 0
    },
    background: {
        backgroundColor: colors.neutral,
    },
    title: {
        color: colors.oxblood,
        textDecoration: "underline",
        fontFamily: 'Bangers',
        padding: 1
    }
};
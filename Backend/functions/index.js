// firebase emulators:start --import=./emulatordata

const admin = require('firebase-admin');
const serviceAccount = require("../serviceAccount.json")
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://reimapf-default-rtdb.europe-west1.firebasedatabase.app"
});

const functions = require("firebase-functions");

const gitlabApi = require("./Gitlab/gitlab")
const conf = require("./conf");


exports.fetchGitlab = functions.https.onRequest(async (request, response) => {
    const gitlab = new gitlabApi(conf.gitlabUser, conf.gitlabURL)

    let gitlabrepos = await gitlab.getRepositories();

    let projects = [];

    for (const repository of gitlabrepos) {
        let info = await gitlab.getReposityoryInfo(repository.id);
        if (!info)
            continue;

        info.git = repository.web_url;
        if (info.visible)
            projects.push(info);
    }

    var ref = admin.database().ref("/projects/git");
    await ref.set(projects);

    response.sendStatus(200);
});


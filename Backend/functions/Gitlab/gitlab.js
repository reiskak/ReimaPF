var https = require('https');

class GitlabApi {
    constructor(username, url) {
        this.username = username;
        this.apibase = url;
    }

    async getRepositories() {
        let data = await this.requestJSON(`/api/v4/users/${this.username}/projects`);
        return data;
    }

    async getReposityoryInfo(repositoryId) {
        let data = await this.requestJSON(`/api/v4/projects/${repositoryId}/repository/files/info.json?ref=main`);
        if (!data.content)
            return null;
        let buffer = Buffer.from(data.content, 'base64');
        let json = buffer.toString('utf8');
        return JSON.parse(json);

    }

    async requestJSON(url) {
        var options = {
            host: this.apibase,
            port: 443,
            path: url,
            method: 'GET',
            headers: { 'user-agent': 'node.js' }
        };
        return new Promise((resolve, reject) => {
            https.get(options, (res) => {
                var body = '';
                res.on("data", function (chunk) {
                    body += chunk.toString('utf8');
                });
                res.on("end", function () {
                    try {
                        let data = JSON.parse(body);
                        resolve(data);
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            });
        });
    }
}

module.exports = GitlabApi